#!/bin/bash

if [ ! -f "$HOME/.ssh/id_rsa.pub" ]; then
    >&2 echo 'You need a public ssh key, run ssh-keygen first.'
    exit 1
fi

SSH_PUB_KEY=$(cat $HOME/.ssh/id_rsa.pub)

echo 'Bringing up the containers'
docker-compose -f docker-compose-dev.yml up -d
echo


for server in web db stage
do
    echo "Setting your pubkey to $server"
    docker-compose -f docker-compose-dev.yml exec $server bash -c "echo '$SSH_PUB_KEY' > /root/.ssh/authorized_keys"
    docker-compose -f docker-compose-dev.yml exec $server bash -c "chmod -R go-rwx /root/.ssh"
done

